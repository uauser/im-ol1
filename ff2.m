clear all
close all
N = 20;ii=1;

%for N=10:10:60
h = 1/N;
e = ones(N,1);
A1d = -(1/h^2)*spdiags([e -2*e e ],[-1,0,1],N,N);
A2d = kron(A1d,speye(N))+ kron(speye(N),A1d);



%b1d = ones(N,1)
b1d = zeros(N,1);
b1d(N/2)=1;
b = kron(b1d,b1d);
x = h*(1:N);
figure;
imagesc(reshape(b,N,N));
figure
[X,Y] = meshgrid(x,x);
mesh(X,Y,reshape(b,N,N));

%% Calculate the solution with \ (LU factorisation)
sol = A2d\b
figure;
mesh(X,Y,reshape(sol,N,N))
figure;
imagesc(reshape(sol,N,N))

%% Bereken de inverse van de diagonaal elementen van A
Dinv = spdiags(1./diag(A2d),[0],N*N,N*N);
% Jacobi Iteratie Matrix
% M = I - D^-1*A
M = speye(N*N)-Dinv*A2d;

%%
maxit=5000
x = rand(N*N,1); % Random initialisatie
prev = norm(x);
k=1;
tol = 1e-2;
for k=1:maxit
    x = M*x + Dinv*b; % Pas de matrix M toe op schatting
    residu = norm(b-A2d*x);
    error = norm(x-sol);
    errorJac(k) = error;
    resJac(k) = residu;
    st(ii) = k % Sla iteratie op
        
    if (norm(x)/prev < tol)
        break;
    end
end
k
si(ii) = N; % Sla het aantal gridpunten op. (per dimensie)
% Toon de oplossing
figure(2);imagesc(reshape(x,N,N))

 %% Chebyshev iteration
            rho = max(eig(M)) % Bereken de spectrale straal
            
            y0 = rand(N*N,1) % Doe een random initialisatie
            prev = norm(y0)
            y1 = M*y0 % Pas de matrix toe
            mu0 = 1 % Initialiseer mu0 and mu1
            mu1 = 1/(rho-.5)

            for k=1:maxit

                mu2 = (2/rho)*mu1 - mu0
                y2 = 2*(mu1/(rho*mu2))*M*y1-(mu0/mu2)*y0 + 2*(mu1/(rho*mu2))*Dinv*b;
                
                y0 = y1;
                y1 = y2;
                mu0 = mu1;
                mu1 = mu2;
                resCheb(k) = norm(A2d*y2-b);
                errorCheb(k) =norm(y2-sol);
                if (norm(A2d*y2-b)/prev < tol) break; % Check convergence based on residual decrease
                end
            end
            st2(ii)=k
            %ii=ii+1;
            
%


            %
            figure
            hold all
            plot(log10(resCheb/resCheb(1)),'-')
            plot(log10(errorCheb/errorCheb(1)),'-')
            plot(log10(resJac/resJac(1)),'-')
            plot(log10(errorJac/errorJac(1)),'-')
            xlabel('iteration')
            ylabel('$log10(||e^{(k)}||/||e^{(0)}||)$','Interpreter','Latex')
            legend('res Cheb','error Cheb','res Jacobi','error Jacobi','Location','SouthWest')
            